#!/bin/bash
#MSUB -A b1042
#MSUB -q genomics
#MSUB -l walltime=24:00:00
#MSUB -m a
#MSUB -j oe
#MSUB -W umask=0113
#MSUB -l nodes=1:ppn=10  #$p
#MSUB -N QC_RSeQC_cov_tin
#module purge all
module load bowtie2
module load samtools/1.2
module unload mpi/openmpi-1.6.3-gcc-4.6.3
module load R/3.2.2
module load python/anaconda
module load parallel
########################################################
#               coverage plots and TIN using RSeQC     #
########################################################
refbedfile=/projects/b1038/anno/mm10/mm10_RefSeq.bed
bamdirectory="/projects/b1038/Pulmonary/Workspace/testbam"
#cd  $bamdirectory # to bam files that are present
cd $bamdirectory
mkdir -p RSeQCreport
#Outputdirectory =$bamdirectory/RSeQCreport
echo 'estimating gene body coverage'
geneBody_coverage.py -r $refbedfile -i $bamdirectory -o bamdirectory/RSeQCreport #$Outputdirectory
echo '###########################---------coverage estimation finished-----------###############'
echo 'estimating transcript integrity'
tin.py -r $refbedfile -i $bamdirectory # default parameters
echo '###########################---------TIN estimation finished-----------###############'
